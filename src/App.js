import React,{Component} from 'react';
import logo from './logo.svg';
import './App.css';
import customer from '../src/smile/task.json';
//component
import Tasks from '../src/component/tasks';
import Taskform  from "./component/taskform";
// console.log(customer);
import Posts from "./component/posts";
import {
  BrowserRouter as Router,
  Route,
  Link,
  browserHistory,
  Switch
} from 'react-router-dom';
 class App extends React.Component{
   state={
     customer:customer
   }
   addCustomer=(firstname,lastname)=>{
     console.log("adding a new customer");
     console.log(firstname,lastname);
     const newcusto={
       firstname:firstname,
       lastname:lastname,
       id:this.state.customer.length
       
      }
      // consol
      
      this.setState({
        customer:[...this.state.customer,newcusto]
      })
      console.log(newcusto);
    }
    
    deleteChange=(id)=>{
      console.log(this.state.customer);
        const del=  this.state.customer.filter(customer=>customer.id !==id );
        this.setState({
            customer:del
        })
        console.log(del);
  }
  checkDonew=(id)=>{

   const result= this.state.customer.map(data=>{
      if(data.id==id){
        data.done=!data.done
      }
      return data;
    });
    this.setState({
      customer:result
    })
    
  }
  



    render(){
    return <div>
        
        <Router>
          <Link to="/">Home</Link>
          <br/>
          <Link to="/posts">Post</Link>
         <Route exact path="/" render={()=>{
          return  <div>

              <Taskform newcustomer={this.addCustomer}/>
              <Tasks customer={this.state.customer} deletetask={this.deleteChange} checkdata={this.checkDonew}/>
              {/* <Posts/> */}

            </div>
         
          
        }}>

        </Route>
        <Route path="/posts" component={Posts}></Route>
          </Router>
          </div>
    }
 }
// const Helloworld =(props)=>{
//   console.log(props);
//     return(
//       <div id="hello">
//         <h3>{props.myhead}</h3>
//         {props.mytext}</div>
//     );
// }

// class Helloworld extends React.Component{
//   state={
//     show:true
//   }
//  toggleShow=()=>{
//     this.setState({show:!this.state.show})
//   }

//   render(){
//     if(this.state.show){
//       return(
//         <div id="hello">
//           <h3>{this.props.myhead}</h3>
//           {this.props.mytext}
//            <button onClick={this.toggleShow}>Toogle show</button> 
//         </div>
//       )
//     }else{
//      return  <h3>no hay elementos
//        <button onClick={this.toggleShow}>
//       toggle show
//        </button>
//      </h3>
//     }
    

//   }
// }

// function App() {
//   return (
//     // <div className="App">
//       <div>this is my component :: <Helloworld mytext="hello jhonn" myhead="lorem ipsom"/>
//         <Helloworld mytext="hello world" myhead="nueva cabezera"/>
//         <Helloworld mytext="hello!" myhead="new head"/>
//         </div>
//     // </div>
//   );
// }

export default App;
