import React ,{Component} from 'react';

class Posts extends Component{
    
    state={
        posts:[]
    }
   async componentDidMount(){

      

        const client=await fetch( "https://jsonplaceholder.typicode.com/posts");

        const data= await client.json();
        console.log(data);
        this.setState({posts:data});
   
    }

    render(){
    return <div><h1>Public</h1>{
         this.state.posts.map(e=>{
             return <div key={e.id}>
                 <h1>{e.title}</h1>
                 <p>{e.body}</p>
             </div>
         })
    }</div>
    }
}

export default Posts;