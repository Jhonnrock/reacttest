import React,{Component} from 'react';
// import { Customer } from '../../../appbarber/frontend/src/app/models/customer';
import  "./task.css";
import PropTypes from 'prop-types';
class Newtask extends Component{
    stylecolor(){
        return {
            fontSize:'18px',
            color: this.props.customer.done ? 'blue': "yellow",
            textDecoration:'none'
    }
}
   
    render(){
      
        const {customer}=this.props;

        return <div style={this.stylecolor()}>
            <h3>{customer.birthdate}</h3>
            <h3>{customer.cellphone}</h3>
            <h3>{customer.firstname}</h3>
            <h3>{customer.lastname}</h3>
            <h3>{customer.done}</h3>
            <h3>{customer.id}</h3>
            <input type="checkbox" onChange={this.props.checkdata.bind(this,customer.id)} ></input>
             <button style={btndelete} onClick={this.props.deletetask.bind(this,customer.id)}>X</button>
        </div>
    }
}


const btndelete={
    fontSize:'18px',
    background:"blue",
    color:'black',
    border:"none",
    padding:"10px 15px",
    borderRadius:"50%",
    cursor:"pointer"

}

export default Newtask;
